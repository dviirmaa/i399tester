# requires nodejs and java

# in test project directory give following commands:

npm install         # installs libraries

# This command creates directory node_modules inside current directory.
# If on Windows, then delete file node_modules\.bin\phantomjs

npm start           # starts Selenium server

# previous command does not terminate.
# So, open new terminal and run:

npm run hw2tests    # runs tests
